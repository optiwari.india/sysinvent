const { app, BrowserWindow, net } = require('electron')
const btoa=require('btoa')
const si = require('systeminformation')
const serialNumber=require('serial-number');
const api = {
  post: function (data) {
    var body = JSON.stringify(data);
    const request = net.request({
      method: 'POST',
      protocol: 'http:',
      hostname: '192.168.1.200',
      path: '/api/',
      redirect: 'follow',
    })
    request.on('response', (response) => {
      console.log(`STATUS: ${response.statusCode}`)
      console.log(`HEADERS: ${JSON.stringify(response.headers)}`)
      response.on('data', (chunk) => {
        console.log(`BODY: ${chunk}`)
      })
    })
    request.on('finish', () => {
      console.log('Request is Finished')
    })
    request.on('abort', () => {
      console.log('Request is Aborted')
    })
    request.on('error', (error) => {
      console.log(`ERROR: ${JSON.stringify(error)}`)
    })
    request.on('close', (error) => {
      console.log('Last Transaction has occured')
    })
    request.setHeader('Content-Type', 'application/json')
    request.write(body, 'utf-8')
    request.end()
  },
}
function createWindow() {
  serialNumber.useSudo=true;
  
  // serialNumber.preferUUID=true;
  //console.log(serialNumber.toString());
  
  si.cpu().then((r) => {
    api.post({
      cpu: btoa(JSON.stringify(r)),
      
      action: 'update-cpu-info',
    })
  })
  si.system().then((r) => {
    api.post({
      system: btoa(JSON.stringify(r)),
      action: 'update-system-info',
    })
  })
  si.baseboard().then((r) => {
    api.post({
      baseboard: btoa(JSON.stringify(r)),
      action: 'update-baseboard-info',
    })
  })
  si.mem().then((r) => {
    api.post({
      ram: btoa(JSON.stringify(r)),
      action: 'update-ram-info',
    })
  })
  si.users().then((r) => {
    api.post({
      users: btoa(JSON.stringify(r)),
      action: 'update-user-info',
    })
  })
  si.graphics().then((r) => {
    api.post({
      graphics: btoa(JSON.stringify(r)),
      action: 'update-graphics-info',
    })
  })
  si.osInfo().then((r) => {
    api.post({
      osinfo: btoa(JSON.stringify(r)),
      action: 'update-osInfo-info',
    })
  })
  si.networkInterfaces().then((r) => {
    api.post({
      network: btoa(JSON.stringify(r)),
      action: 'update-networkInfo-info',
    })
  })
  si.battery().then((r) => {
    if(r.hasbattery){
      api.post({
        battery:btoa(JSON.stringify(r)),
        action:'update-battery-info'
      });
    }
    console.log(r)
  })
  
  /*
  si.chassis().then((r) => {
    console.log(r)
  })
  // si.networkConnections().then(r=>{
    //     console.log(r);
    // })
        si.system().then((r) => {
          console.log(r)
        })
  si.diskLayout().then((r) => {
    console.log(r)
  })
  // si.processes().then(r=>{
  //     console.log(r);
  // })
  //*/
  /* const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      //   preload: path.join(__dirname, 'preload.js')
    },
  })
  mainWindow.loadFile('index.html') */
}

app.whenReady().then(() => {
  createWindow()
  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})
